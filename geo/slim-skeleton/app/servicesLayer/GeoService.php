<?php

namespace App\app\servicesLayer;

use App\app\connectors\ApiConnector;
use App\app\helpers\Helpers;
use App\app\exception\CustomErrorException;

class GeoService
{

    /***
     * @param $request
     * @return Response
     */
    public function getInfoByIp($data)
    {
        if (!empty($data)) {
            $response = (new ApiConnector())->getByIp($data);
        } else {
            throw new CustomErrorException(Helpers::IP_NOT_VALID, 404);
        }
        return $response;
    }


}