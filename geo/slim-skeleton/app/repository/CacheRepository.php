<?php

namespace App\app\repository;

use App\app\servicesLayer\GeoService;
use App\app\struct\IpStruct;

class CacheRepository implements Repository
{

    private $timeCache = 1800;

    /**
     * @var DI Cache.
     */
    private $cache;

    /***
     * CacheRepository constructor.
     * @param $cache
     */
    public function __construct($cache)
    {
        $this->cache = $cache;
    }

    /***
     * @param \App\app\struct\IpStruct $ip
     */
    public function findByIp(IpStruct $ip)
    {
        $long = ip2long($ip->ip);
        $item = $this->cache->getItem((string)$long);
        if ($item->isHit()) {
            return $item->get();
        } else {
            $data = (new GeoService())->getInfoByIp($ip->ip);
            $this->save($data, $item);
            return $data;
        }

    }

    /***
     * @param \App\app\struct\IpStruct $ip
     */
    public function save($data, $item)
    {
        $item->set($data);
        $item->expiresafter($this->timeCache);
        $result = $this->cache->save($item);
        return $result;
    }

}