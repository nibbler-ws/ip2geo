<?php

namespace App\app\repository;

use App\app\struct\IpStruct;

interface Repository
{

    /**
     * @return mixed - return value
     */
    public function findByIp(IpStruct $value);

    /**
     * @return mixed - get value
     */
    public function save($value, $item);

}