<?php
/**
 * Created by PhpStorm.
 * User: nibbler
 * Date: 14.02.2018
 * Time: 0:00
 */


namespace App\app\conf;

class Config
{

    /**
     * @var array
     */
    protected static $config = [];

    /**
     * @param $filename
     */
    public static function get($filename)
    {
        self::$config = json_decode(file_get_contents(__DIR__ .$filename), true);
        return self::$config;
    }
}