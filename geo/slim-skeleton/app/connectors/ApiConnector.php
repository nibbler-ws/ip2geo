<?php

namespace App\app\connectors;

use App\app\conf\Config;
use App\app\exception\CustomErrorException;
use App\app\struct\Response as ResponseObject;
use App\app\helpers\Helpers;
use Slim\Http\Request;
use Slim\Http\Response;

class ApiConnector
{

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;


    private $geoUrl = null;

    /**
     * ApiConnector constructor.
     * @param $config
     */
    public function __construct()
    {
        $config = Config::get('/config.json');
        $this->geoUrl = $config['api']['geoUrl'];
    }


    /***
     * @param $data
     */
    public function getByIp($ip)
    {
        $data = file_get_contents( $this->geoUrl . $ip);
        $parsed = json_decode($data, true);

        if ($response = $this->checkData($parsed)) {
            $response = new ResponseObject(Helpers::SUCCESS,
                                           $parsed['geoplugin_latitude'],
                                           $parsed['geoplugin_longitude'],
                                           $parsed['geoplugin_region'],
                                           $parsed['geoplugin_city']
                            );
        }
        return $response;
    }

    /***
     * @param $data
     * @return bool
     */
    private function checkData($data)
    {
        if ($data['geoplugin_status'] !== 404) {
            return true;
        } else {
            throw new CustomErrorException(Helpers::IP_NOT_FOUND, 404);
        }
    }

}