<?php

namespace App\app\struct;

use App\app\exception\CustomErrorException;
use App\app\helpers\Helpers;

class IpStruct
{

    private $ip = null;

    /**
     * IpStruct constructor.
     * @param $ip
     */
    public function __construct($ip)
    {
        $this->ip = $this->validateIp($ip);
    }

    /**
     *
     */
    public function __get($name)
    {
       return $this->$name;
    }

    /**
     * @param $ip
     * @return bool
     */
    protected function validateIp($ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            return $ip;
        } else {
            throw new CustomErrorException(Helpers::IP_NOT_VALID, 404);
        }
    }
}
