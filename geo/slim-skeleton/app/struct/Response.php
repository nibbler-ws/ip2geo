<?
namespace App\app\struct;

class Response
{

    private $msg,  $lon, $lat, $state, $city = null;

    /***
     * Response constructor.
     * @param $statusCode
     * @param null $lon
     * @param null $log
     * @param null $state
     * @param null $city
     */
    public function __construct($msg, $lat = null, $lon = null, $state = null, $city = null)
    {
            $this->msg = $msg;
            $this->lat = $lat;
            $this->lon = $lon;
            $this->state = $state;
            $this->city = $city;
    }

    /***
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }

}