<?php

namespace App\app\controller;

use App\app\struct\IpStruct;
use Interop\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use App\app\repository\CacheRepository;

class GeoController
{
    /**
     * @var CacheRepository
     */
    private $cacheRepository;

    /***
     * GeoController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->cacheRepository = new CacheRepository($container['cache'], $this->request, $this->response);
    }

    /***
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function findByIp(Request $request, Response $response, array $args)
    {
        $params = $request->getParam('ip');
        $dtoIP = new IpStruct($params);
        $geoService = $this->cacheRepository->findByIp($dtoIP);
        $data = [ 'data' => [
                            'msg' => $geoService->msg,
                            'lat' => $geoService->lat,
                            'lon' => $geoService->lon,
                            'state' => $geoService->state,
                            'city' => $geoService->city,
                        ]
        ];
        return $response->withJson($data, $geoService->statusCode);
    }

}


