<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};


//cache
$container['cache'] = function ($c) {
    $config = [
        'schema' =>'tcp',
        'host' => "redis",
        'port' => 6379,
        'database' => 1
    ];
    $connection = new Predis\Client($config);
    return new Symfony\Component\Cache\Adapter\RedisAdapter($connection);
};

// routing controllers
$container['GeoController'] = function($c) {
    return new App\app\controller\GeoController($c);
};

//Override the default Not Found Handler
$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        if ($exception instanceof \App\app\exception\CustomErrorException) {
            $code = $exception->getCode();
            $message = $exception->getMessage();
            $data = ["error" => ['code' => $code, 'message' => $message]];
            $body = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
            return $response->withStatus($code)->withHeader("Content-type", "application/json")->write($body);
        }

    };
};

// not found routing
$container['notFoundException'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        $code = 404;
        $data = [
            "status" => $code,
            "message" => 'method not found',
        ];
        $body = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        return $response->withStatus($code)->withHeader("Content-type", "application/json")->write($body);
    };
};

// not allowed
$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        $code = 405;
        $data = [
            "status" => $code,
            "message" => 'method not allowed',
        ];
        $body = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        return $response->withStatus($code)->withHeader("Content-type", "application/json")->write($body);
    };
};